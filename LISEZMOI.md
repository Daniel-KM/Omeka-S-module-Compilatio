Compilatio (module pour Omeka S)
================================

> __Les nouvelles versions de ce modules et l’assistance pour Omeka S version 3.0
> et supérieur sont disponibles sur [GitLab], qui semble mieux respecter les
> utilisateurs et la vie privée que le précédent entrepôt.__

See [English readme].

[Compilatio] est un module pour [Omeka S] qui permet d’envoyer des documents
avec des fichiers au service [Compilatio](https://compilatio.net), qui compile
les travaux étudiants et des chercheurs dans une grande base de données pour
détecter les plagiats.

Les réponses de Compilatio ne sont pas gérées (données sur les passages plagiés),
car le plagiat est normalement contrôlé en amont du dépôt final dans un entrepot
institutionnel.

Une clé d’API est requise pour déposer les documents sur le service Compilatio.


Installation
------------

Consulter la documentation utilisateur pour [installer un module].

Le module dépend du module [Common], qui doit être installé en premier.
Le module [Easy Admin] est nécessaire pour créer et lancer des tâches.

Le module utilise une bibliothèque externe et il faut donc soit utiliser le zip
publié ou initialiser les sources.

* À partir du zip

Télécharger la dernière livraison [Compilatio.zip] depuis la liste des
livraisons (le zip depuis "master" ne contient pas les dépendances) et
décompresser la dans le dossier `modules`.

* Depuis la source et pour le développement

Si le module est installé depuis la source, renommez le nom du dossier du module
en `Compilatio`, allez à la racine du module et lancer :

```sh
composer install --no-dev
```

* La dépendance [econoa/nusoap] installée par composer et utilisée pour gérer
les requêtes pour Compilatio a été choisie car il s’agit d’un client SOAP simple
(contrairement à [phpro/soap-client]) et elle **n’oblige pas** à installer
l’extension [php-soap] sur le serveur.


Utilisation
-----------

- Configurer le module :
  - Indiquer la clé d’api.
  - Si souhaité, indiquer la propriété où enregistrer l’url du rapport.
- Lancer uen tâche (ou via [Easy Admin]) :
  - Enregistrer les urls manquantes dans la propriété sélectionnée, par exemple
    quand des identifiants Compilatio sont ajoutés directement dans la base.
  - Envoyer des fichiers à Comilatio, du moins ceux ayant les taille, extension
    et media-type corrects.

Les tâches sont égalemnt disponibles via [Easy Admin].
Une tâche régulière ("cron") peut être ajoutée via le module [Easy Admin].


TODO
----

- [ ] Envoyer à compilatio lors de l’enregistrement ? Sans doute pas.
- [x] Créer une tâche pour remplir les uri quand l’id Compilatio est connue.
- [ ] Créer une tâche pour remplir la table compilatio quand l’uri est connue.


Attention
---------

Utilisez-le à vos propres risques.

Il est toujours recommandé de sauvegarder vos fichiers et vos bases de données
et de vérifier vos archives régulièrement afin de pouvoir les reconstituer si
nécessaire.


Dépannage
---------

Voir les problèmes en ligne sur la page des [questions du module] du GitLab.


Licence
-------

### Module

Ce module est publié sous la licence [CeCILL v2.1], compatible avec [GNU/GPL] et
approuvée par la [FSF] et l’[OSI].

Ce logiciel est régi par la licence CeCILL de droit français et respecte les
règles de distribution des logiciels libres. Vous pouvez utiliser, modifier
et/ou redistribuer le logiciel selon les termes de la licence CeCILL telle que
diffusée par le CEA, le CNRS et l’INRIA à l’URL suivante "http://www.cecill.info".

En contrepartie de l’accès au code source et des droits de copie, de
modification et de redistribution accordée par la licence, les utilisateurs ne
bénéficient que d’une garantie limitée et l’auteur du logiciel, le détenteur des
droits patrimoniaux, et les concédants successifs n’ont qu’une responsabilité
limitée.

À cet égard, l’attention de l’utilisateur est attirée sur les risques liés au
chargement, à l’utilisation, à la modification et/ou au développement ou à la
reproduction du logiciel par l’utilisateur compte tenu de son statut spécifique
de logiciel libre, qui peut signifier qu’il est compliqué à manipuler, et qui
signifie donc aussi qu’il est réservé aux développeurs et aux professionnels
expérimentés ayant des connaissances informatiques approfondies. Les
utilisateurs sont donc encouragés à charger et à tester l’adéquation du logiciel
à leurs besoins dans des conditions permettant d’assurer la sécurité de leurs
systèmes et/ou de leurs données et, plus généralement, à l’utiliser et à
l’exploiter dans les mêmes conditions en matière de sécurité.

Le fait que vous lisez actuellement ce document signifie que vous avez pris
connaissance de la licence CeCILL et que vous en acceptez les termes.

### Dependencies

- [ecunoa/nusoap]: LGPL-2.0-only


Copyright
---------

* Copyright Daniel Berthereau, 2022-2024 (voir [Daniel-KM] sur GitLab)

Voir le copyright des dépendances dans le dossier "vendor/".

La première version de ce module a été conçue pour l’entrepôt institutionnel des
travaux étudiants [Dante] de l’[Université de Toulouse Jean-Jaurès].


[Compilatio]: https://gitlab.com/Daniel-KM/Omeka-S-module-Compilatio
[English readme]: https://gitlab.com/Daniel-KM/Omeka-S-module-Compilatio
[Omeka S]: https://omeka.org/s
[Common]: https://gitlab.com/Daniel-KM/Omeka-S-module-Common
[Easy Admin]: https://gitlab.com/Daniel-KM/Omeka-S-module-EasyAdmin
[Compilatio.zip]: https://gitlab.com/Daniel-KM/Omeka-S-module-Compilatio/-/releases
[installer un module]: https://omeka.org/s/docs/user-manual/modules/#installing-modules
[econoa/nusoap]: https://github.com/pwnlabs/nusoap
[phpro/soap-client]: https://github.com/phpro/soap-client
[php-soap]: https://packagist.org/providers/ext-soap
[module issues]: https://gitlab.com/Daniel-KM/Omeka-S-module-Compilatio/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[Dante]: https://dante.univ-tlse2.fr
[Université de Toulouse Jean-Jaurès]: https://www.univ-tlse2.fr
[GitLab]: https://gitlab.com/Daniel-KM
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
