<?php declare(strict_types=1);

namespace Compilatio;

if (!class_exists(\Common\TraitModule::class)) {
    require_once dirname(__DIR__) . '/Common/TraitModule.php';
}

use Common\Stdlib\PsrMessage;
use Common\TraitModule;
use Laminas\EventManager\Event;
use Laminas\EventManager\SharedEventManagerInterface;
use Laminas\Mvc\Controller\AbstractController;
use Omeka\Module\AbstractModule;

/**
 * Compilatio
 *
 * @copyright Daniel Berthereau, 2022-2024
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */
class Module extends AbstractModule
{
    use TraitModule;

    const NAMESPACE = __NAMESPACE__;

    protected function preInstall(): void
    {
        $services = $this->getServiceLocator();
        $plugins = $services->get('ControllerPluginManager');
        $translate = $plugins->get('translate');

        if (!method_exists($this, 'checkModuleActiveVersion') || !$this->checkModuleActiveVersion('Common', '3.4.54')) {
            $message = new \Omeka\Stdlib\Message(
                $translate('The module %1$s should be upgraded to version %2$s or later.'), // @translate
                'Common', '3.4.54'
            );
            throw new \Omeka\Module\Exception\ModuleCannotInstallException((string) $message);
        }
    }

    public function attachListeners(SharedEventManagerInterface $sharedEventManager): void
    {
        $sharedEventManager->attach(
            \EasyAdmin\Form\CheckAndFixForm::class,
            'form.add_elements',
            [$this, 'handleJobsForm']
        );
        $sharedEventManager->attach(
            \EasyAdmin\Controller\CheckAndFixController::class,
            'easyadmin.job',
            [$this, 'handleEasyAdminJobs']
        );
    }

    public function handleConfigForm(AbstractController $controller)
    {
        $result = parent::handleConfigForm($controller);
        if (!$result) {
            return false;
        }

        $params = $controller->getRequest()->getPost();
        if (empty($params['compilatio_process_send']) && empty($params['compilatio_process_append_url'])) {
            return true;
        }

        $services = $this->getServiceLocator();
        $dispatcher = $services->get(\Omeka\Job\Dispatcher::class);

        if (!empty($params['compilatio_process_append_url'])) {
            $job = $dispatcher->dispatch(\Compilatio\Job\AppendCompilatioUrl::class, []);
            $this->jobMessage($job, $controller->translate('Append Compilatio url')); // @translate
        }

        if (!empty($params['compilatio_process_send'])) {
            // Synchronous dispatcher for quick testing purpose.
            // $job = $dispatcher->dispatch(\Compilatio\Job\SendToCompilatio::class, [], $services->get('Omeka\Job\DispatchStrategy\Synchronous'));
            $job = $dispatcher->dispatch(\Compilatio\Job\SendToCompilatio::class, []);
            $this->jobMessage($job, $controller->translate('Send to Compilatio')); // @translate
        }

        return true;
    }

    public function handleJobsForm(Event $event): void
    {
        /**
         * @var \EasyAdmin\Form\CheckAndFixForm $form
         * @var \Laminas\Form\Element\Radio $process
         */
        $form = $event->getTarget();
        $fieldset = $form->get('module_tasks');
        $process = $fieldset->get('process');
        $valueOptions = $process->getValueOptions();
        $valueOptions['compilatio_append_url'] = 'Append missing Compilatio url to medias'; // @translate
        $valueOptions['compilatio_send'] = 'Send supported files to Compilatio'; // @translate
        $process->setValueOptions($valueOptions);
    }

    public function handleEasyAdminJobs(Event $event): void
    {
        $process = $event->getParam('process');
        if ($process === 'compilatio_append_url') {
            $event->setParam('job', \Compilatio\Job\AppendCompilatioUrl::class);
        } elseif ($process === 'compilatio_send') {
            $event->setParam('job', \Compilatio\Job\SendToCompilatio::class);
        }
    }

    protected function jobMessage(\Omeka\Entity\Job $job, string $task): void
    {
        $plugins = $this->getServiceLocator()->get('ControllerPluginManager');
        $urlPlugin = $plugins->get('url');
        $messenger = $plugins->get('messenger');

        $message = new PsrMessage(
            'Processing task "{task}" in background (job {link_job}#{job_id}{link_end}, {link_log}logs{link_end).', // @translate
            [
                'task' => $task,
                'link_job' => sprintf(
                    '<a href="%s">',
                    htmlspecialchars($urlPlugin->fromRoute('admin/id', ['controller' => 'job', 'id' => $job->getId()]))
                ),
                'job_id' => $job->getId(),
                'link_end' => '</a>',
                'link_log' => sprintf(
                    '<a href="%s">',
                    // Check if module Log is enabled (avoid issue when disabled).
                    htmlspecialchars(class_exists(\Log\Module::class)
                        ? $urlPlugin->fromRoute('admin/log/default', [], ['query' => ['job_id' => $job->getId()]])
                        : $urlPlugin->fromRoute('admin/id', ['controller' => 'job', 'id' => $job->getId(), 'action' => 'log'])
                )),
            ]
        );
        $message->setEscapeHtml(false);
        $messenger->addSuccess($message);
    }
}
