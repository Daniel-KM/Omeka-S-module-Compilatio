<?php declare(strict_types=1);

namespace Compilatio;

return [
    'form_elements' => [
        'invokables' => [
            Form\ConfigForm::class => Form\ConfigForm::class,
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => dirname(__DIR__) . '/language',
                'pattern' => '%s.mo',
                'text_domain' => null,
            ],
        ],
    ],
    'compilatio' => [
        'config' => [
            'compilatio_api_key' => null,
            'compilatio_property_store_url' => null,
            'compilatio_value_private' => false,
            'compilatio_store_set' => 'all',
        ],
    ],
];
