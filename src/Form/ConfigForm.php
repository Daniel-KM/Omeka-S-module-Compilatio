<?php declare(strict_types=1);

namespace Compilatio\Form;

use Laminas\Form\Element;
use Laminas\Form\Form;
use Omeka\Form\Element as OmekaElement;

class ConfigForm extends Form
{
    public function init(): void
    {
        $this
            ->add([
                'name' => 'compilatio_api_key',
                'type' => Element\Text::class,
                'options' => [
                    'label' => 'Compilatio api key', // @translate
                ],
                'attributes' => [
                    'id' => 'compilatio_api_key',
                ],
            ])
            ->add([
                'name' => 'compilatio_property_store_url',
                'type' => OmekaElement\PropertySelect::class,
                'options' => [
                    'label' => 'Media property to store the Compilatio url for the file', // @translate
                    'info' => 'The property may be "dcterms:isReferencedBy" or "dcterms:isRequiredBy", or anything else.', // @translate
                    'empty_option' => 'No property: do not store Compilatio url', // @translate
                    'term_as_value' => true,
                ],
                'attributes' => [
                    'id' => 'compilatio_property_store_url',
                    'class' => 'chosen-select',
                ],
            ])
            ->add([
                'name' => 'compilatio_value_private',
                'type' => Element\Checkbox::class,
                'options' => [
                    'label' => 'Store url as a private value', // @translate
                ],
                'attributes' => [
                    'id' => 'compilatio_value_private',
                ],
            ])
            ->add([
                'name' => 'compilatio_store_set',
                'type' => Element\Radio::class,
                'options' => [
                    'label' => 'Select files to process', // @translate
                    'value_options' => [
                        'all' => 'All supported files', // @translate
                        'only' => 'Only files with value "Compilatio" in property above (label or uri), but without compilatio url', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'compilatio_store_set',
                ],
            ])
            ->add([
                'name' => 'compilatio_process_append_url',
                'type' => Element\Checkbox::class,
                'options' => [
                    'label' => 'Start process to append missing Compilatio urls in the property above', // @translate
                ],
                'attributes' => [
                    'id' => 'compilatio_process_append_url',
                ],
            ])
            ->add([
                'name' => 'compilatio_process_send',
                'type' => Element\Checkbox::class,
                'options' => [
                    'label' => 'Start process to send supported files to Compilatio', // @translate
                ],
                'attributes' => [
                    'id' => 'compilatio_process_send',
                ],
            ])
        ;

        $inputFilter = $this->getInputFilter();
        $inputFilter->add([
            'name' => 'compilatio_property_store_url',
            'required' => false,
        ]);
    }
}
