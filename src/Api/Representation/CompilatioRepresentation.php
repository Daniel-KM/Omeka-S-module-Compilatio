<?php declare(strict_types=1);

namespace Compilatio\Api\Representation;

use DateTime;
use Omeka\Api\Representation\AbstractEntityRepresentation;
use Omeka\Api\Representation\MediaRepresentation;

class CompilatioRepresentation extends AbstractEntityRepresentation
{
    public function resourceName(): string
    {
        return 'compilatios';
    }

    // public function getControllerName()
    // {
    //     return 'compilatio';
    // }

    public function getJsonLdType()
    {
        return 'o-module-compilatio:Compilatio';
    }

    public function getJsonLd()
    {
        $media = $this->media();

        $created = [
            '@value' => $this->getDateTime($this->created()),
            '@type' => 'http://www.w3.org/2001/XMLSchema#dateTime',
        ];

        return [
            'o:id' => $this->id(),
            'o:media' => $media ? $media->getReference() : null,
            'o-module-compilatio:document_id' => $this->documentId(),
            'o:created' => $created,
        ];
    }

    public function media(): ?MediaRepresentation
    {
        $media = $this->resource->getMedia();
        return $media
            ? $this->getAdapter('media')->getRepresentation($media)
            : null;
    }

    public function documentId(): string
    {
        return $this->resource->getDocumentId();
    }

    public function created(): DateTime
    {
        return $this->resource->getCreated();
    }
}
