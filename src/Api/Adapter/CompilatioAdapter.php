<?php declare(strict_types=1);

namespace Compilatio\Api\Adapter;

use Doctrine\ORM\QueryBuilder;
use Omeka\Api\Adapter\AbstractEntityAdapter;
use Omeka\Api\Request;
use Omeka\Entity\EntityInterface;
use Omeka\Stdlib\ErrorStore;

class CompilatioAdapter extends AbstractEntityAdapter
{
    protected $sortFields = [
        'id' => 'id',
        'media' => 'media',
        'document_id' => 'documentId',
        'created' => 'created',
    ];

    protected $scalarFields = [
        'id' => 'id',
        'media' => 'media',
        'document_id' => 'documentId',
        'created' => 'created',
    ];

    public function getResourceName()
    {
        return 'compilatios';
    }

    public function getRepresentationClass()
    {
        return \Compilatio\Api\Representation\CompilatioRepresentation::class;
    }

    public function getEntityClass()
    {
        return \Compilatio\Entity\Compilatio::class;
    }

    public function buildQuery(QueryBuilder $qb, array $query): void
    {
        $expr = $qb->expr();

        if (isset($query['item_id'])) {
            if (!is_array($query['item_id'])) {
                $query['item_id'] = [$query['item_id']];
            }
            $mediaAlias = $this->createAlias();
            $qb->innerJoin(
                'omeka_root.media',
                $mediaAlias
            );
            $qb->andWhere($expr->in(
                $mediaAlias . '.item',
                $this->createNamedParameter($qb, $query['item_id'])
            ));
        }

        if (isset($query['media_id'])) {
            if (!is_array($query['media_id'])) {
                $query['media_id'] = [$query['media_id']];
            }
            $mediaAlias = $this->createAlias();
            $qb->innerJoin(
                'omeka_root.media',
                $mediaAlias
            );
            $qb->andWhere($expr->in(
                $mediaAlias . '.id',
                $this->createNamedParameter($qb, $query['media_id'])
            ));
        }

        if (isset($query['document_id'])) {
            $qb->andWhere($expr->eq(
                'omeka_root.documentId',
                $this->createNamedParameter($qb, $query['document_id'])
            ));
        }

        // TODO Add time comparison (see modules AdvancedSearchPlus or Next).
        if (isset($query['created'])) {
            $qb->andWhere($expr->eq(
                'omeka_root.created',
                $this->createNamedParameter($qb, $query['created'])
            ));
        }
    }

    public function validateRequest(Request $request, ErrorStore $errorStore): void
    {
        $data = $request->getContent();
        if (empty($data['o:media']) || empty($data['o:media']['o:id'])) {
            $errorStore->addError('o:media', 'A media is required.'); // @translate
        } else {
            /** @var \Omeka\Entity\Media $media */
            $media = $this->getAdapter('media')->findEntity($data['o:media']['o:id']);
            if (!$media) {
                $errorStore->addError('o:media', 'An existing media is required.'); // @translate
            } elseif (!$media->getFilename()) {
                $errorStore->addError('o:media', 'The media must have an original file.'); // @translate
            }
        }
        if (empty($data['o-module-compilatio:document_id'])) {
            $errorStore->addError('o-module-compilatio:document_id', 'The Compilatio document id is required.'); // @translate
        } elseif (strlen((string) $data['o-module-compilatio:document_id']) !== 32
            || !ctype_xdigit((string) $data['o-module-compilatio:document_id'])
        ) {
            $errorStore->addError('o-module-compilatio:document_id', 'The Compilatio document id must be a 32 characters hexadecimal value.'); // @translate
        }
    }

    public function hydrate(Request $request, EntityInterface $entity, ErrorStore $errorStore): void
    {
        // TODO Use shouldHydrate() and validateEntity().
        /** @var \Compilatio\Entity\Compilatio $entity */
        $data = $request->getContent();
        if (Request::CREATE === $request->getOperation()) {
            $media = $this->getAdapter('media')->findEntity($data['o:media']['o:id']);
            $documentId = strtolower((string) $data['o-module-compilatio:document_id']);
            $entity
                ->setMedia($media)
                ->setDocumentId($documentId)
                ->setCreated(new \DateTime('now'));
        }
        // No update is possible, only deletion.
        // Media may be set null automatically.
    }
}
