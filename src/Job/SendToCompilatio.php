<?php declare(strict_types=1);

namespace Compilatio\Job;

use Compilatio\Entity\Compilatio;
use DateTime;
use Omeka\Entity\Media;
use Omeka\Job\AbstractJob;

class SendToCompilatio extends AbstractJob
{
    use TraitCompilatio;

    public function perform(): void
    {
        if (!$this->prepare()) {
            return;
        }

        $this->process();

        unset($this->soapClient);
    }

    protected function process()
    {
        $translate = $this->getServiceLocator()->get('ControllerPluginManager')->get('translate');

        // Get all medias with a file not already sent.
        // To query media file is not possible via api, so use direct query.
        $qb = $this->connection->createQueryBuilder();
        $expr = $qb->expr();
        $qb
            ->select('media.id', 'CONCAT(media.storage_id, IF (media.extension = "" OR media.extension IS NULL, "", CONCAT(".", media.extension)))')
            ->from('media')
            ->innerJoin('media', 'resource', 'item', 'item.id = media.item_id')
            ->leftJoin('media', 'compilatio', 'compilatio', 'compilatio.media_id = media.id')
            ->where($expr->eq('item.is_public', 1))
            // ->andWhere($expr->eq('media.is_public', 1))
            ->andWhere($expr->isNull('compilatio.media_id'))
            ->andWhere($expr->eq('media.has_original', 1))
            ->andWhere($expr->isNotNull('media.size'))
            ->andWhere($expr->lte('media.size', $this->sizeLimit))
            ->andWhere($expr->in('media.extension', ':extensions'))
            ->andWhere($expr->in('media.media_type', ':media_types'))
        ;

        $bind = [
            'extensions' => $this->allowedExtensions,
            'media_types' => $this->allowedMediaTypes,
        ];
        $types = [
            'extensions' => \Doctrine\DBAL\Connection::PARAM_STR_ARRAY,
            'media_types' => \Doctrine\DBAL\Connection::PARAM_STR_ARRAY,
        ];

        if ($this->storeSet === 'only') {
            $qb
                // Limit medias to those with a value of type "uri", with value
                // or label "compilatio", but no url.
                ->innerJoin('media', 'value', 'value', 'value.resource_id = media.id AND value.property_id = ' . $this->propertyStoreUrlId . ' AND value.type = "uri"')
                ->andWhere($expr->orX(
                    $expr->eq('value.uri', ':compilatio'),
                    $expr->andX(
                        $expr->eq('value.value', ':compilatio'),
                        $expr->orX($expr->isNull('value.uri'), $expr->notLike('value.uri', ':compilatio_url'))
                    )
                ));
            $bind['compilatio'] = 'compilatio';
            $bind['compilatio_url'] = 'https://www.compilatio.net/%';
            $types['compilatio'] = \Doctrine\DBAL\ParameterType::STRING;
            $types['compilatio_url'] = \Doctrine\DBAL\ParameterType::STRING;
        }

        $filenames = $this->connection->executeQuery($qb, $bind, $types)->fetchAllKeyValue();
        $this->logger->notice(
            'There are {count} public item’s supported files whose size is lower than {size} bytes and not yet processed.', // @translate
            ['count' => count($filenames), 'size' => $this->sizeLimit]
        );

        if ($this->storeSet === 'only') {
            $this->logger->notice(
                'Only medias with an uri value "compilatio" without url are included.' // @translate
            );
        }

        if (empty($filenames)) {
            return true;
        }

        $dateTime = new DateTime('now');
        $msgNoType = $translate('No document type'); // @translate
        $msgUnknownError = $translate('Unknown error.'); // @translate
        $propertyStoreUrl = null;

        $idx = 0;
        $totalProcessed = 0;
        foreach (array_chunk($filenames, 100, true) as $chunk) {
            if ($this->shouldStop()) {
                $this->logger->notice(
                    'Job stopped: {processed}/{total} processed.', // @translate
                    ['processed' => $totalProcessed, 'total' => count($filenames)]
                );
                return false;
            }

            // Reinit property when entity manager is cleared.
            if ($this->propertyStoreUrlId && !$propertyStoreUrl) {
                $propertyStoreUrl = $this->entityManager->getReference(\Omeka\Entity\Property::class, $this->propertyStoreUrlId);
            }

            foreach ($chunk as $id => $filename) {
                $filepath = $this->basePath . '/original/' . $filename;

                // Check if file exists.
                if (!file_exists($filepath) || !is_file($filepath) || !filesize($filepath) || !is_readable($filepath)) {
                    $this->logger->err(
                        'Media #{media_id}: file missing, empty or not readable.', // @translate
                        ['media_id' => $id]
                    );
                    continue;
                }

                // Check wrong file size.
                $filesize = filesize($filepath);
                if ($filesize > $this->sizeLimit) {
                    $this->logger->err(
                        'Media #{media_id}: the file size ({file_size}) is greater than {size} and it is stored incorrectly in the database.', // @translate
                        ['media_id' => $id, 'file_size' => $filesize, 'size' => $this->sizeLimit]
                    );
                    continue;
                }

                $media = $this->entityManager->getReference(Media::class, $id);
                /** @var \Omeka\Api\Representation\MediaRepresentation $mediaRepr */
                $mediaRepr = $this->mediaAdapter->getRepresentation($media);
                $itemRepr = $mediaRepr->item();
                $title = $itemRepr->displayTitle();
                // $description = $itemRepr->displayDescription($title);
                $description = $title;
                $resourceClass = $itemRepr->resourceClass();
                $docType = $resourceClass ? $resourceClass->label() : $msgNoType; // @translate
                $mediaType = $media->getMediaType();

                // Send file to Compilatio. All data are already utf-8.
                // To get the format (requests and responses), check output of
                // the Compilatio soap url (wsdl).
                $data = [
                    'key' => $this->compilatioApiKey,
                    'title' => $title,
                    'description' => "[$docType] $description",
                    'filename' => basename($filename),
                    'mimetype' => $mediaType,
                    'content' => $this->base64EncodeFile($filepath),
                    'idFolder' => '',
                    'lmsUserEmail' => '',
                ];
                $result = $this->soapClient->call('addDocumentBase64', $data);

                // Store the new document id on success.
                if ($result && !is_array($result)) {
                    $idCompilatio = $result;
                    $compilatio = new Compilatio();
                    $compilatio
                        ->setMedia($media)
                        ->setDocumentId($idCompilatio)
                        ->setCreated($dateTime);
                    $this->entityManager->persist($compilatio);
                    $this->logger->info(
                        'Media #{media_id}: successfully stored in Compilatio with id {id_compilatio}.', // @translate
                        ['media_id' => $id, 'id_compilatio' => $idCompilatio]
                    );
                    if ($propertyStoreUrl) {
                        $this->storeReportUrl($media, $idCompilatio);
                    }
                } else {
                    $msg = is_array($result) && isset($result['faultstring']) ? $result['faultstring'] : $msgUnknownError;
                    $this->logger->err(
                        'Media #{media_id}: error when sending file to Compilatio: {msg}', // @translate
                        ['media_id' => $id, 'msg' => $msg]
                    );
                }
            }

            $this->entityManager->flush();
            $this->entityManager->clear();

            $idx += count($chunk);
            $this->logger->notice(
                'Processed {count}/{total} media files.', // @translate
                ['count' => $idx, 'total' => count($filenames)]
            );
        }

        $this->logger->notice(
            'Process ended.' // @translate
        );
    }

    /**
     * Encode a checked file into base64.
     *
     * @link https://www.php.net/manual/en/function.base64-encode.php#111942
     */
    protected function base64EncodeFile(string $filepath): ?string
    {
        if (!file_exists($filepath) || !is_file($filepath) || !is_readable($filepath)) {
            return null;
        }

        $output = '';
        $fh = fopen($filepath, 'rb');
        while (!feof($fh)) {
            $plain = fread($fh, 57 * 143);
            $encoded = base64_encode($plain);
            $encoded = chunk_split($encoded, 76, "\r\n");
            // fwrite($outputFile, $encoded);
            $output .= $encoded;
        }
        return $output;
    }
}
