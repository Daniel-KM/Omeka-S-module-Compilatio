<?php declare(strict_types=1);

namespace Compilatio\Job;

use Omeka\Entity\Media;
use Omeka\Job\AbstractJob;

class AppendCompilatioUrl extends AbstractJob
{
    use TraitCompilatio;

    public function perform(): void
    {
        if (!$this->prepare()) {
            return;
        }

        if (empty($this->propertyStoreUrlId)) {
            $this->logger->err(
                'The property where to store the url is not set.' // @translate
            );
            unset($this->soapClient);
            return;
        }

        $this->process();

        unset($this->soapClient);
    }

    protected function process()
    {
        $translate = $this->getServiceLocator()->get('ControllerPluginManager')->get('translate');

        // Get all medias with a file sent and a compilatio id, but without
        // filled value.
        // To query media file is not possible via api, so use direct query.
        $qb = $this->connection->createQueryBuilder();
        $expr = $qb->expr();
        $qb
            ->select('media.id', 'compilatio.documentId')
            ->from('media')
            ->innerJoin('media', 'compilatio', 'compilatio', 'compilatio.media_id = media.id')
            ->leftJoin('media', 'value', 'value', 'value.resource_id = media.id AND value.property_id = ' . $this->propertyStoreUrlId . ' AND value.type = "uri"')
            ->where($expr->andX($expr->isNotNull('media.id'), $expr->isNull('value.id')))
            ->orWhere($expr->eq('value.uri', ':compilatio'))
            ->orWhere($expr->andX(
                $expr->eq('value.value', ':compilatio'),
                $expr->orX($expr->isNull('value.uri'), $expr->notLike('value.uri', ':compilatio_url'))
            ));
        $bind = [
            'compilatio' => 'compilatio',
            'compilatio_url' => 'https://www.compilatio.net/%',
        ];
        $types = [
            'compilatio' => \Doctrine\DBAL\ParameterType::STRING,
            'compilatio_url' => \Doctrine\DBAL\ParameterType::STRING,
        ];

        $ids = $this->connection->executeQuery($qb, $bind, $types)->fetchAllKeyValue();

        if (empty($ids)) {
            $this->logger->notice(
                'There is no compilatio id to append to medias.' // @translate
            );
            return true;
        }

        $idx = 0;
        $totalProcessed = 0;
        foreach (array_chunk($ids, 100, true) as $chunk) {
            if ($this->shouldStop()) {
                $this->logger->notice(
                    'Job stopped: {processed}/{total} processed.', // @translate
                    ['processed' => $totalProcessed, 'total' => count($ids)]
                );
                return false;
            }

            foreach ($chunk as $idMedia => $idCompilatio) {
                $media = $this->entityManager->getReference(Media::class, $idMedia);
                $this->storeReportUrl($media, $idCompilatio);
            }

            $this->entityManager->flush();
            $this->entityManager->clear();

            $idx += count($chunk);
            $this->logger->notice(
                'Processed {count}/{total} compilatio ids.', // @translate
                ['count' => $idx, 'total' => count($ids)]
            );
        }

        if ($this->hasLog) {
            $this->logger->notice(
                'Process ended.' // @translate
            );
        } else {
            $this->logger->notice(
                $translate('Process ended.') // @translate
            );
        }
    }
}
