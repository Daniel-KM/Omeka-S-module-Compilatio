<?php declare(strict_types=1);

namespace Compilatio\Job;

require_once __DIR__ . '/../../vendor/autoload.php';

use nusoap_client;
use Omeka\Entity\Media;
use Omeka\Entity\Resource;
use Omeka\Entity\Value;

trait TraitCompilatio
{
    /**
     * @var string
     */
    protected $compilatioUrl = 'https://service.compilatio.net/webservices/CompilatioUserClient.wsdl';

    /**
     * @var \Laminas\Log\Logger
     */
    protected $logger;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $connection;

    /**
     * @var \Omeka\Api\Manager
     */
    protected $api;

    /**
     * @var \Omeka\Api\Adapter\MediaAdapter;
     */
    protected $mediaAdapter;

    /**
     * @var string
     */
    protected $basePath;

    /**
     * @var nusoap_client
     */
    protected $soapClient;

    /**
     * @var string
     */
    protected $compilatioApiKey = '';

    /**
     * @var ?int
     */
    protected $propertyStoreUrlId;

    /**
     * @var bool
     */
    protected $storeValuePublic = true;

    /**
     * @var string
     */
    protected $storeSet = 'all';

    /**
     * @var int
     */
    protected $sizeLimit = 0;

    /**
     * @var string[]
     */
    protected $allowedExtensions = [];

    /**
     * @var string[]
     */
    protected $allowedMediaTypes = [];

    protected function prepare(): bool
    {
        $services = $this->getServiceLocator();

        // The reference id is the job id for now.
        $referenceIdProcessor = new \Laminas\Log\Processor\ReferenceId();
        $referenceIdProcessor->setReferenceId('compilatio/send/' . $this->job->getId());

        $this->logger = $services->get('Omeka\Logger');
        $this->logger->addProcessor($referenceIdProcessor);
        $this->api = $services->get('Omeka\ApiManager');
        $this->entityManager = $services->get('Omeka\EntityManager');
        $this->connection = $services->get('Omeka\Connection');
        $this->mediaAdapter = $services->get('Omeka\ApiAdapterManager')->get('media');

        $config = $services->get('Config');
        $this->basePath = $config['file_store']['local']['base_path'] ?: (OMEKA_PATH . '/files');

        // Connexion to Compilatio.
        $this->soapClient = new nusoap_client($this->compilatioUrl, 'wsdl');
        $this->soapClient->soap_defencoding = 'UTF-8';
        // Not working; require "text/xml".
        // $this->soapClient->contentType = 'application/soap+xml';
        // $this->soapClient->use_curl = true;

        if (!$this->prepareConfig()) {
            unset($this->soapClient);
            return false;
        }

        return true;
    }

    protected function prepareConfig(): bool
    {
        $services = $this->getServiceLocator();
        $settings = $services->get('Omeka\Settings');

        $this->compilatioApiKey = $settings->get('compilatio_api_key');
        if (!$this->compilatioApiKey) {
            $this->logger->err(
                'The compilatio api key is not set.' // @translate
            );
            return false;
        }

        $translate = $services->get('ControllerPluginManager')->get('translate');

        $result = $this->soapClient->call('getAllowedFileMaxSize', [
            'key' => $this->compilatioApiKey,
        ]);
        if ($result && is_array($result) && !isset($result['faultstring'])) {
            $this->sizeLimit = $result['octets'] ?? 0;
        } else {
            $msg = is_array($result) && isset($result['faultstring']) ? $result['faultstring'] : $translate('Unknown error.');
            $this->logger->err(
                'The Compilatio file size limit is not defined: {message}.', // @translate
                ['message' => $msg]
            );
            return false;
        }

        $result = $this->soapClient->call('getAllowedFileTypes', [
            'key' => $this->compilatioApiKey,
        ]);
        if ($result && is_array($result) && !empty($result['CompilatioFileType'])) {
            foreach ($result['CompilatioFileType'] as $filetype) {
                $this->allowedExtensions[] = $filetype['type'] ?? null;
                $this->allowedMediaTypes[] = $filetype['mimetype'] ?? null;
            }
            $this->allowedExtensions = array_unique(array_filter($this->allowedExtensions));
            $this->allowedMediaTypes = array_unique(array_filter($this->allowedMediaTypes));
        } else {
            $msg = is_array($result) && isset($result['faultstring']) ? $result['faultstring'] : $translate('Unknown error.');
            $this->logger->err(
                'The Compilatio list of allowed media types is not defined: {message}.', // @translate
                ['message' => $msg]
            );
            return false;
        }

        $propertyStoreUrl = $settings->get('compilatio_property_store_url');
        if ($propertyStoreUrl) {
            /** @var \Omeka\Api\Adapter\MediaAdapter $adapter */
            $adapter = $services->get('Omeka\ApiAdapterManager')->get('media');
            $this->propertyStoreUrlId = $adapter->getPropertyByTerm($propertyStoreUrl);
            $this->propertyStoreUrlId = $this->propertyStoreUrlId ? (int) $this->propertyStoreUrlId->getId() : null;
        }

        $this->storeSet = $this->propertyStoreUrlId && $settings->get('compilatio_store_set') === 'only'
            ? 'only'
            : 'all';

        $this->storeValuePublic = !$settings->get('compilatio_value_private');

        return true;
    }

    protected function getValueStore(Resource $resource): ?Value
    {
        if (empty($this->propertyStoreUrlId)) {
            return null;
        }
        // TODO Use doctrine matching filters.
        /** @var \Omeka\Entity\Value value */
        foreach ($resource->getValues() as $value) {
            $propId = $value->getProperty()->getId();
            if ($propId === $this->propertyStoreUrlId && $value->getType() === 'uri') {
                $label = mb_strtolower(trim((string) $value->getValue()));
                $uri = mb_strtolower(trim((string) $value->getUri()));
                if ($uri === 'compilatio'
                    || ($label === 'compilatio' && mb_strpos($uri, 'https://www.compilatio.net/') === false)
                ) {
                    return $value;
                }
            }
        }
        return null;
    }

    protected function storeReportUrl(Media $media, string $idCompilatio): bool
    {
        static $msgUnknownError;
        static $propertyStoreUrl;

        if (empty($this->propertyStoreUrlId) || empty($idCompilatio)) {
            return false;
        }

        if (is_null($msgUnknownError)) {
            $translate = $this->getServiceLocator()->get('ControllerPluginManager')->get('translate');
            $msgUnknownError = $translate('Unknown error.'); // @translate
        }

        // Reinit property when entity manager is cleared.
        $propertyStoreUrl = $this->entityManager->getReference(\Omeka\Entity\Property::class, $this->propertyStoreUrlId);

        $result = $this->soapClient->call('getDocumentReportUrl', [
            'key' => $this->compilatioApiKey,
            'idDocument' => $idCompilatio,
        ]);

        $isSuccess = $result && !is_array($result);
        if ($isSuccess) {
            if ($this->storeSet === 'only') {
                $value = $this->getValueStore($media) ?? new Value();
            } else {
                $value = new Value();
            }
            $value
                ->setResource($media)
                ->setProperty($propertyStoreUrl)
                ->setType('uri')
                ->setIsPublic($this->storeValuePublic)
                ->setValue('Compilatio')
                ->setUri($result);
            $this->entityManager->persist($value);
        } else {
            $msg = is_array($result) && isset($result['faultstring']) ? $result['faultstring'] : $msgUnknownError;
            $this->logger->err(
                'Media #{media_id}: unable to store the report url : {message}', // @translate
                ['media_id' => $media->getId(), 'message' => $msg]
            );
        }

        return $isSuccess;
    }
}
