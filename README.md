Compilatio (module for Omeka S)
===============================

> __New versions of this module and support for Omeka S version 3.0 and above
> are available on [GitLab], which seems to respect users and privacy better
> than the previous repository.__

Voir le [Lisez-moi] en français.

[Compilatio] is a module for [Omeka S] that allows to send items with files to
[Compilatio](https://compilatio.net), a service that compilates student and
researcher works in a big database to detect plagiarism.

The output of Compilatio is not managed (data about plagied texts), because the
plagiarism is normally checked sooner than when a work is deposit in a
institutional repository.

An api key is required to deposit files in Compilatio service.


Installation
------------

See general end user documentation for [installing a module].

This module is dependant of module [Common], that should be installed first.
The module [Easy Admin] is needed to create and run tasks.

The module uses an external library, so use the release zip to install it, or
use and init the source.

* From the zip

Download the last release [Compilatio.zip] from the list of releases
(the "master" does not contain the dependency), and uncompress it in the `modules`
directory.

* From the source and for development

If the module was installed from the source, rename the name of the folder of
the module to `Compilatio`, go to the root module, and run:

```sh
composer install --no-dev
```

* The dependency [econoa/nusoap] installed by composer and used to managed SOAP
requests to Compilatio has been selected because it a simple SOAP client (unlike
[phpro/soap-client]) and it **does not require** to install the extension [php-soap]
on the server.


Usage
-----

- Config the module:
  - Set the api key.
  - If wanted, set the property where to store the url to the report.
  - Set if you want to send all managed files to Compilatio or only marked ones.
- Process one of the jobs (or via [Easy Admin]):
  - Store missing urls in the selected property, for example when new Compilatio
    ids are added directly in the database.
  - Send files to Compilatio, that is all medias matching requirements (size, file
    extension, media type).

The tasks are available in [Easy Admin] too.
A cron task may be added via the module [Easy Admin].


TODO
----

- [ ] Send to compilatio on save? Probably not.
- [x] Create a job to fill the uri when the Compilatio Id are already known.
- [ ] Create a job to fill the table of Compilatio Id when uri is already known.


Warning
-------

Use it at your own risk.

It’s always recommended to backup your files and your databases and to check
your archives regularly so you can roll back if needed.


Troubleshooting
---------------

See online issues on the [module issues] page on GitLab.


License
-------

### Module

This module is published under the [CeCILL v2.1] license, compatible with
[GNU/GPL] and approved by [FSF] and [OSI].

This software is governed by the CeCILL license under French law and abiding by
the rules of distribution of free software. You can use, modify and/ or
redistribute the software under the terms of the CeCILL license as circulated by
CEA, CNRS and INRIA at the following URL "http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy, modify and
redistribute granted by the license, users are provided only with a limited
warranty and the software’s author, the holder of the economic rights, and the
successive licensors have only limited liability.

In this respect, the user’s attention is drawn to the risks associated with
loading, using, modifying and/or developing or reproducing the software by the
user in light of its specific status of free software, that may mean that it is
complicated to manipulate, and that also therefore means that it is reserved for
developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software’s suitability as
regards their requirements in conditions enabling the security of their systems
and/or data to be ensured and, more generally, to use and operate it in the same
conditions as regards security.

The fact that you are presently reading this means that you have had knowledge
of the CeCILL license and that you accept its terms.

### Dependencies

- [econoa/nusoap] : LGPL-2.0-only


Copyright
---------

* Copyright Daniel Berthereau, 2022-2024 (see [Daniel-KM] on GitLab)

See copyright for dependencies in directory "vendor/".

First version of this module was done for the institutional repository of
student works [Dante] of the [Université de Toulouse Jean-Jaurès].


[Compilatio]: https://gitlab.com/Daniel-KM/Omeka-S-module-Compilatio
[Lisez-moi]: https://gitlab.com/Daniel-KM/Omeka-S-module-Compilatio/-/blob/master/LISEZMOI.md
[Omeka S]: https://omeka.org/s
[Common]: https://gitlab.com/Daniel-KM/Omeka-S-module-Common
[Easy Admin]: https://gitlab.com/Daniel-KM/Omeka-S-module-EasyAdmin
[Compilatio.zip]: https://gitlab.com/Daniel-KM/Omeka-S-module-Compilatio/-/releases
[installing a module]: https://omeka.org/s/docs/user-manual/modules/#installing-modules
[econoa/nusoap]: https://github.com/pwnlabs/nusoap
[phpro/soap-client]: https://github.com/phpro/soap-client
[php-soap]: https://packagist.org/providers/ext-soap
[module issues]: https://gitlab.com/Daniel-KM/Omeka-S-module-Compilatio/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[Dante]: https://dante.univ-tlse2.fr
[Université de Toulouse Jean-Jaurès]: https://www.univ-tlse2.fr
[GitLab]: https://gitlab.com/Daniel-KM
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
